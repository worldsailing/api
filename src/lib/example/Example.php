<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */
namespace worldsailing\Api\example;

use worldsailing\Api\auth\TokenRequest;
use worldsailing\Api\core\AbstractResourceRequest;
use worldsailing\Api\core\Curl;
use worldsailing\Api\core\ResourceRequestInterface;
use worldsailing\Api\core\RestProtocolResultHandler;
use worldsailing\Api\core\Token;
use worldsailing\Api\core\TokenLevel;
use worldsailing\Api\core\WsApiException;

/**
 * Class Example
 * @package worldsailing\Api\example
 */
class Example extends AbstractResourceRequest implements ResourceRequestInterface
{

    /**
     * @var string
     */
    protected static $tokenLevel = TokenLevel::LEVEL1;

    /**
     * @var string
     */
    protected static $url = 'example/';

    /**
     * If input parameter is null, then creates a new token and set it up to static::storedToken, otherwise set the given value.
     * Returns with value of static::storedToken
     *
     * @param Token|null $token
     * @return Token
     */
    public static function token(Token $token = null)
    {
        if ($token) {
            static::$storedToken = $token;
        } else {
            static::$storedToken = TokenRequest::create(static::$tokenLevel);
        }
        return static::$storedToken;
    }

    /**
     * If static::storedToken is null, then creates a new token, set it up to $soredToken otherwise does not creates a new one.
     * Returns with value of static::storedToken
     *
     * @return Token
     */
    public static function getToken()
    {
        if (static::$storedToken === null) {
            return static::token();
        } else {
            return static::$storedToken;
        }
    }


    /**
     * @param array $arguments
     * @param bool $autoRefresh
     * @return string
     * @throws WsApiException
     */
    public static function getList($arguments = [], $autoRefresh = false)
    {

        static::$method = 'GET';

        $url = self::getRootUrl('example') . static::$url;

        $requiredArguments = ['limit', 'page'];

        foreach ($requiredArguments as $element) {
            if (in_array($element, array_keys($arguments)) === false) {
                throw new WsApiException('Missing required arguments [' . $element . ']');
            }
        }

        $url .= static::prepareUrlElement('?', 'limit', ((int) $arguments['limit'] > 0) ? $arguments['limit'] : 25);

        $url .= static::prepareUrlElement('&', 'page', ((int) $arguments['page'] > 0) ? $arguments['page'] : 1);

        if (isset($arguments['order'])) {
            $url .= static::prepareUrlElement('&', 'order', $arguments['order']);
        }

        if (isset($arguments['search'])) {
            $url .= static::prepareUrlElement('&', 'search', $arguments['search']);
        }

        //refresh token if necessary
        if (! static::isStoredToken()) {
            static::getToken();
        } else {
            if ($autoRefresh === true) {
                static::token();
            }
        }

        $curl = new Curl(static::$logger);
        /** @var \worldsailing\Api\core\RestProtocolResult $result */
        static::$lastRestResponse = $curl->init()
            ->setMethod(static::$method)
            ->setUrl($url)
            ->setHeader(['Authorization: ' . ((static::$storedToken->getValue()->token_type) . ' ' . (static::$storedToken->getValue()->access_token) )])
            ->run(static::isSsl(), 1);

        if (RestProtocolResultHandler::isValid(static::$lastRestResponse)) {
            return static::$lastRestResponse->getBody();
        } else {
            if (static::$storedToken->isCached() && (! $autoRefresh)) {
                return static::getList($arguments, true);
            } else {
                throw new WsApiException((RestProtocolResultHandler::error() != '') ? RestProtocolResultHandler::error() : 'An error occurred', RestProtocolResultHandler::code());
            }
        }
    }

    /**
     * @param array $arguments
     * @param bool $autoRefresh
     * @return string
     * @throws WsApiException
     */
    public static function getEntity($arguments = [], $autoRefresh = false)
    {

        static::$method = 'GET';

        $url = self::getRootUrl('example') . static::$url;

        $url .= static::prepareUrlSegment((isset($arguments['id'])) ? (int) $arguments['id'] : 0);

        //refresh token if necessary
        if (! static::isStoredToken()) {
            static::getToken();//creates a new one on normal way
        } else {
            if ($autoRefresh === true) {
                static::token();//creates a new one anyway
            }
        }

        $curl = new Curl(static::$logger);
        /** @var \worldsailing\Api\core\RestProtocolResult $result */
        static::$lastRestResponse = $curl->init()
            ->setMethod(static::$method)
            ->setUrl($url)
            ->setHeader(['Authorization: ' . ((static::$storedToken->getValue()->token_type) . ' ' . (static::$storedToken->getValue()->access_token) )])
            ->run(static::isSsl(), 1);

        if (RestProtocolResultHandler::isValid(static::$lastRestResponse)) {
            return static::$lastRestResponse->getBody();
        } else {
            if (static::$storedToken->isCached() && (! $autoRefresh)) {
                return static::getEntity($arguments, true);
            } else {
                throw new WsApiException((RestProtocolResultHandler::error() != '') ? RestProtocolResultHandler::error() : 'An error occurred', RestProtocolResultHandler::code());
            }
        }
    }

    /**
     * @param array $arguments
     * @return string
     * @throws WsApiException
     */
    public static function create($arguments = [])
    {

        static::$method = 'POST';

        $url = self::getRootUrl('example') . static::$url;

        //New token anyway
        static::token();

        $curl = new Curl(static::$logger);
        /** @var \worldsailing\Api\core\RestProtocolResult $result */
        static::$lastRestResponse = $curl->init()
            ->setMethod(static::$method)
            ->setUrl($url)
            ->setData($arguments)
            ->setHeader(['Authorization: ' . ((static::$storedToken->getValue()->token_type) . ' ' . (static::$storedToken->getValue()->access_token) )])
            ->run(static::isSsl(), 1);

        if (RestProtocolResultHandler::isValid(static::$lastRestResponse, [200,201])) {
            return static::$lastRestResponse->getBody();
        } else {
            throw new WsApiException((RestProtocolResultHandler::error() != '') ? RestProtocolResultHandler::error() : 'An error occurred' , RestProtocolResultHandler::code());
        }
    }

    /**
     * @param array $arguments
     * @return string
     * @throws WsApiException
     */
    public static function save($arguments = [])
    {

        static::$method = 'PUT';

        $url = self::getRootUrl('example') . static::$url;

        $url .= static::prepareUrlSegment((isset($arguments['id'])) ? (int) $arguments['id'] : 0);

        //New token anyway
        static::token();

        $curl = new Curl(static::$logger);
        /** @var \worldsailing\Api\core\RestProtocolResult $result */
        static::$lastRestResponse = $curl->init()
            ->setMethod(static::$method)
            ->setUrl($url)
            ->setData($arguments)
            ->setHeader(['Authorization: ' . ((static::$storedToken->getValue()->token_type) . ' ' . (static::$storedToken->getValue()->access_token) )])
            ->run(static::isSsl(), 1);

        if (RestProtocolResultHandler::isValid(static::$lastRestResponse, [200,202])) {
            return static::$lastRestResponse->getBody();
        } else {
            throw new WsApiException((RestProtocolResultHandler::error() != '') ? RestProtocolResultHandler::error() : 'An error occurred', RestProtocolResultHandler::code());
        }
    }

    /**
     * @param array $arguments
     * @return string
     * @throws WsApiException
     */
    public static function delete($arguments = [])
    {

        static::$method = 'DELETE';

        $url = self::getRootUrl('example') . static::$url;

        $url .= static::prepareUrlSegment((isset($arguments['id'])) ? (int) $arguments['id'] : 0);

        //New token anyway
        static::token();

        $curl = new Curl(static::$logger);
        /** @var \worldsailing\Api\core\RestProtocolResult $result */
        static::$lastRestResponse = $curl->init()
            ->setMethod(static::$method)
            ->setUrl($url)
            ->setData($arguments)
            ->setHeader(['Authorization: ' . ((static::$storedToken->getValue()->token_type) . ' ' . (static::$storedToken->getValue()->access_token) )])
            ->run(static::isSsl(), 1);

        if (RestProtocolResultHandler::isValid(static::$lastRestResponse, [200, 204])) {
            return static::$lastRestResponse->getBody();
        } else {
            throw new WsApiException((RestProtocolResultHandler::error() != '') ? RestProtocolResultHandler::error() : 'An error occurred', RestProtocolResultHandler::code());
        }
    }
}

