<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace worldsailing\Api\response\example;

use worldsailing\Common\ApiResultSet\fieldType\CollectionFieldType;
use worldsailing\Common\ApiResultSet\ListResultSet;

class ExampleList extends ListResultSet
{
    public function describe($resource)
    {
        $this->vars = new CollectionFieldType('Examples', function ($item) {
            return new ExampleEntity('Example', $item);
        }, $resource);
    }
}

