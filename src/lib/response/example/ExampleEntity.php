<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace worldsailing\Api\response\example;

use worldsailing\Common\ApiResultSet\EntityResultSet;
use worldsailing\Common\ApiResultSet\fieldType\IntegerFieldType;
use worldsailing\Common\ApiResultSet\fieldType\StringFieldType;

class ExampleEntity extends EntityResultSet
{

    public function describe($resource)
    {
        $this->vars = [
            new IntegerFieldType('BiogMembId', $resource->getBiogMembId()),
            new StringFieldType('BiogIsafId', $resource->getBiogIsafId()),
            new StringFieldType('BiogFirstName', $resource->getBiogFirstName()),
            new StringFieldType('BiogSurname', $resource->getBiogSurname()),
            new StringFieldType('BiogEmail', $resource->getBiogEmail()),
            new StringFieldType('CreatedAt', $resource->getCreatedAt()),
            new StringFieldType('UpdatedAt', $resource->getUpdatedAt()),
        ];
    }
}

