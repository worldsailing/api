<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace worldsailing\Api\response\xrr;

use worldsailing\Api\response\ErrorEntity;
use worldsailing\Common\ApiResultSet\EntityResultSet;
use worldsailing\Common\ApiResultSet\fieldType\CollectionFieldType;

/**
 * Class XrrImportResultSet
 * @package Model\xrr\api
 */
class XrrImportResultSet extends EntityResultSet
{
    public function describe($resource)
    {
        $this->vars = [
            new XrrReportResultSet('report', $resource),
            new CollectionFieldType('error', function ($item) {
                if ($item instanceof ErrorEntity) {
                    return $item;
                } else {
                    return new ErrorEntity('error', $item);
                }

            }, $resource['error'])
        ];
    }
}
