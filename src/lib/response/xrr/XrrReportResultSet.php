<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace worldsailing\Api\response\xrr;

use worldsailing\Common\ApiResultSet\EntityResultSet;
use worldsailing\Common\ApiResultSet\fieldType\CollectionFieldType;

/**
 * Class XrrReportResultSet
 * @package Model\xrr\api
 */
class XrrReportResultSet extends EntityResultSet
{
    public function describe($resource)
    {
        $this->vars = [
            new XrrValidationResultSet('validation', $resource['validation'])//,
            //new XrrPersistenceResultSet('persistence', $resource['persistence']),
        ];
    }
}
