<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace worldsailing\Api\response\xrr;

use worldsailing\Common\ApiResultSet\EntityResultSet;
use worldsailing\Common\ApiResultSet\fieldType\LogicalStringFieldType;


class XrrValidationResultSet extends EntityResultSet
{
    public function describe($resource)
    {
        $this->vars = [
            new LogicalStringFieldType('structure', $resource['structure'] ),
            new LogicalStringFieldType('schema', $resource['schema'] ),
            new LogicalStringFieldType('persons', $resource['persons'] ),
            new LogicalStringFieldType('boats', $resource['boats'] ),
            new LogicalStringFieldType('teams', $resource['teams'] ),
            new LogicalStringFieldType('events', $resource['events'] )
        ];
    }
}
