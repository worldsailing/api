<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace worldsailing\Api\response;

use Symfony\Component\Validator\ConstraintViolationInterface;
use worldsailing\Common\ApiResultSet\fieldType\CollectionFieldType;
use worldsailing\Common\ApiResultSet\ListResultSet;

class ValidationErrorList extends ListResultSet
{
    public function describe($resource)
    {
        /** @var ConstraintViolationInterface $item */
        $this->vars = new CollectionFieldType('errors', function ($item) {
            if ($item instanceof ConstraintViolationInterface) {
                return new ErrorEntity('error', [
                    'type' => 'validation_error',
                    'code' => $item->getCode(),
                    'message' => $item->getMessage(),
                    'context' => $item->getPropertyPath()
                ]);
            } else {
                throw new \Exception('Type mismatch [Validation error must be a valid ConstraintViolationInterface instance]');
            }

        }, $resource);
    }
}

