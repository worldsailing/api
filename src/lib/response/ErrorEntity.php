<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace worldsailing\Api\response;

use worldsailing\Common\ApiResultSet\EntityResultSet;
use worldsailing\Common\ApiResultSet\fieldType\StringFieldType;

class ErrorEntity extends EntityResultSet
{

    public function describe($resource)
    {
        $this->vars = [
            new StringFieldType('type', $resource['type']),
            new StringFieldType('code', $resource['code']),
            new StringFieldType('message', $resource['message']),
            new StringFieldType('context', (isset($resource['context']) ? $resource['context'] : ''))
        ];
    }
}

