<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */
namespace worldsailing\Api\auth;

use worldsailing\Api\core\AbstractTokenRequest;
use worldsailing\Api\core\Curl;
use worldsailing\Api\core\RestProtocolResultHandler;
use worldsailing\Api\core\Token;
use worldsailing\Api\core\TokenLevel;
use worldsailing\Api\core\WsApiException;

class TokenRequest extends AbstractTokenRequest
{

    /**
     * @param string $tokenLevel
     * @param Token|null $level1Token
     * @return void|Token
     * @throws WsApiException
     */
    public static function create($tokenLevel, Token $level1Token = null)
    {
        switch ($tokenLevel) {
            case TokenLevel::LEVEL1 :
                return static::createLevel1();
                break;
            case TokenLevel::LEVEL2 :
                return static::createLevel2($level1Token);
                break;
            default :
                throw new WsApiException('Invalid token level [' . $tokenLevel . ']');
                break;
        }
    }

    /**
     * @return Token
     * @throws WsApiException
     */
    public static function createLevel1()
    {
        $url = self::getRootUrl('auth') . 'token/';

        $curl = new Curl(static::$logger);
        /** @var \worldsailing\Api\core\RestProtocolResult $result */
        $result = $curl->init()
                    ->setMethod(self::$method)
                    ->setUrl($url)
                    ->setData(array('grant_type' => 'client_credentials'))
                    ->run(static::isSsl(), 0 , static::$config->clientId, static::$config->secret);

        if (RestProtocolResultHandler::isValid($result, [200,201,202])) {
            $token = new Token($result->getHeader(), false);
        } else {
            throw new WsApiException((RestProtocolResultHandler::error() != '') ? RestProtocolResultHandler::error() : 'An error occurred', RestProtocolResultHandler::code());
        }

        return $token;
    }


    /**
     * @param Token|null $level1Token
     * @return Token
     * @throws WsApiException
     */
    public static function createLevel2(Token $level1Token = null)
    {
        if ($level1Token === null) {
            $level1Token = static::createLevel1();
        }

        $url = self::getRootUrl('auth') . 'token/secure/';

        throw new WsApiException('Not implemented');
    }

}

