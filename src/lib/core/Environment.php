<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */
namespace worldsailing\Api\core;

class Environment extends AbstractEnum
{

    const DEVELOPMENT = 'dev';

    const TEST = 'test';

    const PRODUCTION = 'prod';

}

