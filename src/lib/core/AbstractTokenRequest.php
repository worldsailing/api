<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */
namespace worldsailing\Api\core;

abstract class AbstractTokenRequest extends AbstractBaseRequest
{

    protected static $method = 'POST';


    /**
     * @param string $tokenLevel
     * @return Token
     */
    abstract protected static function create($tokenLevel);

    /**
     * @return Token
     */
    abstract protected static function createLevel1();

    /**
     * @param Token $level1Token
     * @return Token
     */
    abstract protected static function createLevel2(Token $level1Token);

}

