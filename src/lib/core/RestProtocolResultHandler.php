<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */
namespace worldsailing\Api\core;

use worldsailing\Helper\WsHelper;

/**
 * Class RestProtocolResultHandler
 * @package worldsailing\Api\core
 */
class RestProtocolResultHandler
{

    /**
     * @var string
     */
    private static $error = '';
    /**
     * @var int
     */
    private static $code;

    /**
     * @param RestProtocolResult $result
     * @param array $accepted
     * @return bool
     */
    public static function isValid(RestProtocolResult $result, $accepted = [])
    {
        self::$error = '';
        self::$code = $result->code();
        if ($result->isSuccess()
            && (in_array($result->code(), [200, 201, 202, 203, 204, 205, 206]))
            && ((count($accepted) == 0) || (in_array($result->code(), $accepted)))
        ) {
            return true;
        } else {
            try {
                $data = json_decode($result->getBody(), false);

                if ($result->isSuccess()) {
                    if (! is_array($data)) {
                        if (self::getMessageByProp($data, 'error_description')) {
                            self::$error = (string)$data->error_description;
                        } elseif (self::getMessageByProp($data, 'error')) {
                            self::$error = (string)$data->error;
                        } elseif (self::getMessageByProp($result, 'message')) {
                            self::$error = (string)$result->message;
                        } else {
                            self::$error = WsHelper::getResponseMessageByCode(self::code());
                        }
                    } else {
                        foreach ($data as $error) {
                            self::$error .= self::getMessageByErrorEntity($error, 'message');
                        }
                    }
                } else {
                    if (self::getMessageByProp($data, 'error_description')) {
                        self::$error = (string)$data->error_description;
                    } elseif (self::getMessageByProp($data, 'error')) {
                        self::$error = (string)$data->error;
                    } elseif (self::getMessageByProp($result, 'message')) {
                        self::$error = (string)$result->message;
                    } else {
                        self::$error = WsHelper::getResponseMessageByCode(self::code());
                    }
                }
            } catch(\Exception $e) {
                //because $result->getBody() is not a valid json
                self::$error = ($result->message != '') ? $result->message : WsHelper::getResponseMessageByCode(self::code());
            } finally {
                return false;
            }
        }
    }


    /**
     * @param object $result
     * @param string $prop
     * @return bool
     */
    private static function getMessageByProp($result, $prop)
    {
        if (isset($result->{$prop}) && ($result->{$prop} != '')) {
            return true;
        } else {
            return false;
        }
    }


    /**
     * @param array|\stdClass $error
     * @return bool
     * @throws WsApiException
     */
    private static function getMessageByErrorEntity($error)
    {
        if (is_array($error)) {
            $obj = json_decode(json_encode($error), false);
        }elseif (is_object($error)) {
            $obj = $error;
        } else {
            throw new WsApiException('Invalid error structure. [Expected: Object]', self::code());
        }

        if (static::getMessageByProp($obj, 'type')) {

            return self::getMessageOfErrorEntity($obj);

        } else {
            throw new WsApiException('Missing property [type] on [\worldsailing\Api\response\ErrorEntity]', self::code());
        }
    }

    /**
     * @param \stdClass $errorObject
     * @return string
     */
    private static function getMessageOfErrorEntity(\stdClass $errorObject)
    {
        switch ($errorObject->type) {
            case 'validation_error':
                return (isset($errorObject->context) && $errorObject->context != '') ? ($errorObject->context . ' ' . $errorObject->message) : $errorObject->message;
                break;
            default:
                return $errorObject->message;
                break;
        }
    }

    /**
     * @return string
     */
    public static function error()
    {
        return self::$error;
    }

    /**
     * @return int
     */
    public static function code()
    {
        return self::$code;
    }
}

