<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */
namespace worldsailing\Api\core;

/**
 * Class Token
 * @package worldsailing\Api\core
 */
class Token
{

    /**
     * @var mixed
     */
    private $value;


    private $cached;

    /**
     * Token constructor.
     * @param mixed $value
     * @param bool $cached
     */
    public function __construct($value, $cached = false)
    {
        $this->value = (is_object($value)) ? json_encode($value) : $value;;
        $this->cached = $cached;
    }

    /**
     * @return object
     */
    public function getValue()
    {
        return json_decode($this->value, false);
    }

    /**
     * @param $value
     * @param bool $cached
     * @return Token
     */
    public function setValue($value, $cached = false)
    {
        $this->value = (is_object($value)) ? json_encode($value) : $value;
        $this->cached = $cached;
        return $this;
    }

    /**
     * @return bool
     */
    public function isCached()
    {
        return $this->cached;
    }


}

