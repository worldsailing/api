<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */
namespace worldsailing\Api\core;

use Monolog\Logger;


interface ResourceRequestInterface
{
    /**
     * @param Logger $logger
     * @return void
     */
    public static function setLogger(Logger $logger);

    /**
     * @param array $credentials
     * @return void
     */
    public static function setCredentials($credentials);

    /**
     * @param $method
     * @param null $arguments
     * @return mixed|void
     */
    public static function log($method, $arguments = null);

    /**
     * @param string $serviceId
     * @return string
     */
    public static function getRootUrl($serviceId);

    /**
     * @param Token|null $token
     * @return Token
     */
    public static function token(Token $token = null);

    /**
     * @return Token
     */
    public static function getToken();

    /**
     *
     * @return bool
     */
    public static function isStoredToken();


}

