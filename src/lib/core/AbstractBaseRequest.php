<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */
namespace worldsailing\Api\core;

use worldsailing\Helper\WsHelper;
use Monolog\Logger;

/**
 * Class AbstractBaseRequest
 * @package worldsailing\Api\core
 */
abstract class AbstractBaseRequest
{

    /**
     * @var Config
     */
    protected static $config;

    /**
     * @var Logger
     */
    protected static $logger;

    /**
     * @var Token
     */
    protected static $storedToken;

    /**
     * @var RestProtocolResult
     */
    protected static $lastRestResponse;



    /**
     * @var string level1|level2
     */
    protected static $tokenLevel = 'level1';

    /**
     * @var string GET|POST|PUT|DELETE
     */
    protected static $method;

    /**
     * @var
     */
    protected static $url;


    /**
     * @param Logger $logger
     */
    public static function setLogger(Logger $logger)
    {
        static::$logger = $logger;
    }

    /**
     * @param $credentials
     * @throws WsApiException
     */
    public static function setCredentials($credentials)
    {
        try {
            static::$config = new Config($credentials);
        } catch (WsApiException $e) {
            static::log('debug', WsHelper::getExceptionContext($e, __FILE__, __LINE__));
            throw new WsApiException($e->getMessage());
        } catch (\Exception $e) {
            static::log('debug', WsHelper::getExceptionContext($e, __FILE__, __LINE__));
            throw new WsApiException('Configuration error. [' . $e->getMessage() . ']');
        }
    }

    /**
     * @param $method
     * @param null $arguments
     * @return bool|mixed
     */
    public static function log($method, $arguments = null){
        if (static::$logger) {
            if (method_exists(static::$logger, $method)) {
                return call_user_func_array(array(static::$logger, $method), $arguments);
            } else {
                return false;
            }
        }
    }

    /**
     * @return bool
     * @throws WsApiException
     */
    public static function isSsl()
    {
        if ((static::$config === null) || (!static::$config instanceof Config)) {
            throw new WsApiException('Configuration error. [ Missing configuration. Did you set credentials?]');
        }
        return static::$config->ssl[static::$config->environment];
    }

    /**
     * @return RestProtocolResult
     */
    public static function getLastResponse()
    {
        return static::$lastRestResponse;
    }

    /**
     * @param $serviceId
     * @return string
     * @throws WsApiException
     */
    public static function getRootUrl($serviceId)
    {
        if ((static::$config === null) || (! static::$config instanceof Config)) {
            throw new WsApiException('Configuration error. [ Missing configuration. Did you set credentials?]');
        }
        return static::$config->protocol[static::$config->environment] .  static::$config->domains[static::$config->environment][$serviceId];
    }

    /**
     * @param string $prefix
     * @param string $name
     * @param mixed $value
     * @return string
     */
    public static function prepareUrlElement($prefix, $name, $value)
    {
        if (is_array($value)) {
            $element = $prefix . urlDecode(http_build_query([ $name => $value] ));
        } else {
            $element = $prefix . $name . '=' . urlDecode($value);
        }
        return $element;
    }

    /**
     * @param mixed $value
     * @return string
     */
    public static function prepareUrlSegment($value)
    {
        $result = '';
        if (is_array($value)) {
            foreach ($value as $element) {
                $result .= static::prepareUrlSegment($element);
            }
        } else {
            $result = urlDecode($value) . '/';
        }
        return $result;
    }

}
