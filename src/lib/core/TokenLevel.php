<?php

/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */
namespace worldsailing\Api\core;

/**
 * Class TokenLevel
 * @package worldsailing\Api\core
 */
class TokenLevel extends AbstractEnum
{

    const LEVEL1 = 'level1';

    const LEVEL2 = 'level2';

}

