<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */
namespace worldsailing\Api\core;

/**
 * Class WsApiException
 * @package worldsailing\Api\core
 */
class WsApiException extends \Exception
{
    /**
     * @param string $message
     */
    public function __construct($message, $code = 400)
    {
        parent::__construct($message, $code);
    }


    /**
     * @return string
     */
    public function __toString()
    {
        return __CLASS__ . ": {$this->message}\n";
    }
}

