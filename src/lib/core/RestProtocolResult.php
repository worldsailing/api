<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace worldsailing\Api\core;
/**
 * Class RestProtocolResult
 * @package Core
 */
Class RestProtocolResult {

    /**
     * @var bool
     */
    public $success;
    /**
     * @var null
     */
    public $data;
    /**
     * @var int
     */
    public $headerSize;
    /**
     * @var string
     */
    public $url;
    /**
     * @var string
     */
    public $message;

    /**
     * @var mixed
     */
    public $cached;

    /**
     * RestProtocolResult constructor.
     */
    public function __construct()
    {
        $this->code = 200;
        $this->success = true;
        $this->data = null;
        $this->headerSize = 0;
        $this->url = '';
        $this->message = '';
        $this->cached = null;
    }

    /**
     * @return bool
     */
    public function isSuccess()
    {
        return ($this->success === true) ? true : false;
    }

    /**
     * @return null|mixed
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param null $value
     * @return $this
     */
    public function setData($value = null)
    {
        $this->data = $value;
        return $this;
    }

    /**
     * @param null $value
     * @return $this|bool
     */
    public function success($value = null)
    {
        if ($value !== null) {
            $this->success = $value;
            return $this;
        } else {
            return $this->success;
        }
    }

    /**
     * @param null $value
     * @return $this|string
     */
    public function message($value = null)
    {
        if ($value !== null) {
            $this->message = $value;
            return $this;
        } else {
            return $this->message;
        }
    }

    /**
     * @param null $value
     * @return $this|int
     */
    public function headerSize($value = null)
    {
        if ($value !== null) {
            $this->headerSize = $value;
            return $this;
        } else {
            return $this->headerSize;
        }
    }

    /**
     * @param null $value
     * @return $this|string
     */
    public function url($value = null)
    {
        if ($value !== null) {
            $this->url = $value;
            return $this;
        } else {
            return $this->url;
        }
    }


    /**
     * @param null $value
     * @return $this|null|mixed
     */
    public function code($value = null)
    {
        if ($value !== null) {
            $this->code = $value;
            return $this;
        } else {
            return $this->code;
        }
    }

    /**
     * @param null $value
     * @return $this|null|mixed
     */
    public function cached($value = null)
    {
        if ($value !== null) {
            $this->cached = $value;
            return $this;
        } else {
            return $this->cached;
        }
    }

    /**
     * @return string
     */
    public function getHeader()
    {
        return substr($this->getData(), 0, $this->headerSize());
    }


    /**
     * @return string
     */
    public function getBody()
    {
        return substr($this->getData(), $this->headerSize());
    }
}
