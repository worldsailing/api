<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */
namespace worldsailing\Api\core;

/**
 * Class AbstractResourceRequest
 * @package worldsailing\Api\core
 */
abstract class AbstractResourceRequest extends AbstractBaseRequest implements ResourceRequestInterface
{

    /**
     * If input parameter is null, then creates a new token and set it up to static::storedToken, otherwise set the given value.
     * Returns with value of static::storedToken
     *
     * @param Token|null $token
     * @return Token
     */
    abstract public static function token(Token $token = null);

    /**
     * If static::storedToken is null, then creates a new token, set it up to $soredToken otherwise does not creates a new one.
     * Returns with value of static::storedToken
     *
     * @return Token
     */
    abstract public static function getToken();


    /**
     * It returns true if static::$storedToken is not null
     *
     * @return bool
     */
    public static function isStoredToken()
    {
        return (static::$storedToken && (static::$storedToken instanceof Token)) ? true : false;
    }

}

