<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */
namespace worldsailing\Api\core;

/**
 * Class Config
 * @package worldsailing\Api\core
 */
class Config
{

    /**
     * Environment
     *
     * @var string dev|test|prod
     */
    public $environment = 'dev';

    /**
     * Http protocol
     *
     * @var array http://|https://
     */
    public $protocol = [

        Environment::DEVELOPMENT => 'http://',
        Environment::TEST => 'http://',
        Environment::PRODUCTION => 'http://'

    ];

    /**
     * SSL in curl (if protocol is https then it should be true)
     *
     * @var array
     */
    public $ssl = [

        Environment::DEVELOPMENT => false,
        Environment::TEST => false,
        Environment::PRODUCTION => false

    ];

    /**
     * Domains of microservices architecture
     *
     * @var array
     */
    public $domains = [

        Environment::DEVELOPMENT => [

            'auth' => 'authservice.dev/',

            'import' => 'importservice.dev/',

            'example' => 'microservice.dev/'
        ],
        Environment::TEST => [
            'auth' => 'authservice.dev/',

            'import' => 'importservice.dev/',

            'example' => 'microservice.dev/'
        ],
        Environment::PRODUCTION => [
            'auth' => 'authservice.dev/',

            'import' => 'importservice.dev/',

            'example' => 'microservice.dev/'
        ]
    ];

    /**
     * @var string
     */
    public $clientId;

    /**
     * @var string
     */
    public $secret;

    /**
     * @var string
     */
    public $userId;

    /**
     * @var string
     */
    public $password;


    /**
     * Config constructor.
     * @param array $credentials Associated array. Required elements: 'environment', 'client_id', 'secret' Optional elements: 'user_id', 'password', 'token_level'
     * @throws WsApiException
     */
    public function __construct($credentials = [])
    {
        if (isset($credentials['client_id'])) {
            $this->clientId = $credentials['client_id'];
        } else {
            throw new WsApiException('Configuration error. Missing option [client_id]');
        }

        if (isset($credentials['secret'])) {
            $this->secret = $credentials['secret'];
        } else {
            throw new WsApiException('Configuration error. Missing option [secret]');
        }

        if (isset($credentials['environment'])) {
            if (Environment::isValidValue($credentials['environment'])) {
                $this->environment = $credentials['environment'];
            } else {
                throw new WsApiException('Configuration error. Invalid value [environment]');
            }
        } else {
            throw new WsApiException('Configuration error. Missing option [environment]');
        }

        $this->userId = (isset($credentials['user_id']) && $credentials['user_id']) ? $credentials['user_id'] : '';
        $this->password = (isset($credentials['password']) && $credentials['password']) ? $credentials['password'] : '';
    }

}

