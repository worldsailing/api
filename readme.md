# worldsailing API 

### PHP library description
Bundle of REST API to World Sailing microservices based on Silex.

  
### Install by composer
```php
{
  "repositories": [
    {
        "type": "vcs",
        "url":  "https://worldsailing@bitbucket.org/worldsailing/common.git"
    },
    {
        "type": "vcs",
        "url":  "https://worldsailing@bitbucket.org/worldsailing/helper.git"
    },
    {
        "type": "vcs",
        "url":  "https://worldsailing@bitbucket.org/worldsailing/api.git"
    }
  ],
  "require": {
    "worldsailing/common": "dev-master",
    "worldsailing/helper": "dev-master",
    "worldsailing/api": "dev-master"
  }
}
```
 
### License

All right reserved
